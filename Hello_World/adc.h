#ifndef ADC_H
#define ADC_H

int adc_init(void);
int adc_samp(int32_t* buffer, int32_t len);

#endif
