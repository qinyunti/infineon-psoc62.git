  /**
 *****************************************************************************        
 * \brief       平台层(PLATFORM)SHELL命令模块(SHELL_FUN)相关接口实现.
 * \details     
 *              All rights reserved.    
 * \file        shell_fun.c 
 * \author     
 * \version     1.0 
 * \date       
 * \note        平台相关命令.\n
 * \since       新建 
 * \par 修订记录
 * - 初始版本
 * \par 资源说明
 * - RAM:              
 * - ROM:
 *****************************************************************************
 */

#include <stdio.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include <ctype.h>
#include "shell_fun.h"
#include "shell.h"
#include "max.h"
#include "fft.h"
#include "frq.h"
#include "iir.h"
/*****************************************************************************    
 *                                                                           
 *                             内部数据                                   
 *                                                                            
 ****************************************************************************/

const shell_cmd_cfg shell_cmd_list[ ] = 
{
  /*1.帮助相关*/
  { (const uint8_t*)"help",         HelpFun,         "help"},                 /*打印帮助信息*/
  { (const uint8_t*)"max",         MaxFun,           "max"},                 /*打印帮助信息*/
  { (const uint8_t*)"fft",         FftFun,           "fft"},                 /*打印帮助信息*/
  { (const uint8_t*)"frq",         FrqFun,           "frq"},
  { (const uint8_t*)"iir",        IirFun,           "iir"},
  { 0,		0 },
};

/*****************************************************************************    
 *                                                                           
 *                             内部接口函数实现                                   
 *                                                                            
 ****************************************************************************/

/*****************************************************************************    
 *                                                                           
 *                             对外接口函数实现                                   
 *                                                                            
 ****************************************************************************/

/*****************************************************************************    
 *                                                                           
 *                             帮助相关                                  
 *                                                                            
 ****************************************************************************/

void HelpFun(void* param)
{
    unsigned int i;
    printf("\r\n");
    printf("**************\r\n");
    printf("*   SHELL    *\r\n");
    printf("*   V1.0     *\r\n");
    printf("**************\r\n");
    printf("\r\n");
    for (i=0; shell_cmd_list[i].name != 0; i++)
    {
        printf("%02d.",i);
        printf("%-16s",shell_cmd_list[i].name);
        printf("%s\r\n",shell_cmd_list[i].helpstr);
    }
}

void MaxFun(void* param)
{
	max_test();
}

void FftFun(void* param)
{
	for(int i=0;i<100;i++)
	{
		fft_main();
	}
}

void FrqFun(void* param)
{
	for(int i=0;i<100;i++)
	{
		frq_main();
	}
}


void IirFun(void* param)
{
	for(int i=0;i<100;i++)
	{
		iir_main();
	}
}


