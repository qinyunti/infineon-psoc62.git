/**
 *****************************************************************************        
 * \brief       平台层(PLATFORM)SHELL命令模块(SHELL_FUN)相关数据结构和接口描述.
 * \details     
 *              All rights reserved.    
 * \file        shell_fun.h 
 * \author      
 * \version     1.0 
 * \date        
 * \note        平台相关命令.\n
 * \since        新建 
 * \par 修订记录
 * -
 * \par 资源说明
 * - RAM:              
 * - ROM:
 *****************************************************************************
 */

#ifndef __SHELL_FUN_H
#define __SHELL_FUN_H

#ifdef __cplusplus
 extern "C" {
#endif

/*****************************************************************************    
 *                                                                           
 *                             帮助相关                                  
 *                                                                            
 ****************************************************************************/   
void HelpFun(void* param);
void DeadTimeFun(void* param);
void DirFun(void* param);
void SpeedFun(void* param);
void MaxFun(void* param);
void FftFun(void* param);
void FrqFun(void* param);
void IirFun(void* param);
#ifdef __cplusplus
}
#endif

#endif
