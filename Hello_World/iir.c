
#include "arm_math.h"
#include "arm_const_structs.h"
#include <stdio.h>

#define TEST_LENGTH_SAMPLES 2048
#define FS 10000

extern float32_t testInput_f32_10khz[TEST_LENGTH_SAMPLES];
static float32_t testOutput[TEST_LENGTH_SAMPLES];


static uint32_t fftSize = 1024;
static uint32_t ifftFlag = 0;
static uint32_t doBitReverse = 1;
static arm_cfft_instance_f32 varInstCfftF32;

static int testIndex = 0;

static float testtmp_f32_10khz[2048];
static int32_t adcbuffer[2048];


#define numStages  2                /* 2阶IIR滤波的个数 */
#define BLOCK_SIZE           128    /* 调用一次arm_biquad_cascade_df1_f32处理的采样点个数 */


uint32_t blockSize = BLOCK_SIZE;
uint32_t numBlocks = TEST_LENGTH_SAMPLES/BLOCK_SIZE;      /* 需要调用arm_biquad_cascade_df1_f32的次数 */
static float32_t IIRStateF32[4*numStages];                      /* 状态缓存 */

/* 巴特沃斯低通滤波器系数 80Hz*/
const float32_t IIRCoeffs32LP[5*numStages] = {
    1.0f,  2.0f,  1.0f,  1.479798894397216679763573665695730596781f,
-0.688676953053861784503908438637154176831f,

    1.0f,  2.0f,  1.0f,  1.212812092620218384908525877108331769705f,
-0.384004162286553540894828984164632856846f
};

int32_t iir_main(void)
{
    uint32_t i;
    arm_biquad_casd_df1_inst_f32 S;
    float32_t ScaleValue;
    float32_t  *inputF32, *outputF32;

    /* 初始化输入输出缓存指针 */

    //memcpy(testtmp_f32_10khz,testInput_f32_10khz,sizeof(testInput_f32_10khz));
#if 1
    adc_samp(adcbuffer,2048);
    for(int i=0; i<2048;i ++)
   {
  	  testtmp_f32_10khz[i] = (float)adcbuffer[i];
    }
#endif
    inputF32 = testtmp_f32_10khz;
    outputF32 = testOutput;

    /* 初始化 */
    arm_biquad_cascade_df1_init_f32(&S, numStages, (float32_t *)&IIRCoeffs32LP[0],
(float32_t *)&IIRStateF32[0]);

    /* 实现IIR滤波，这里每次处理1个点 */
    for(i=0; i < numBlocks; i++)
    {
        arm_biquad_cascade_df1_f32(&S, inputF32 + (i * blockSize),  outputF32 + (i * blockSize),
  blockSize);
    }

    /*放缩系数 */
    ScaleValue = 0.012f* 0.42f;

    /* 打印滤波后结果 */
    for(i=0; i<TEST_LENGTH_SAMPLES; i++)
    {
        printf("/*%f, %f*/\r\n", testtmp_f32_10khz[i], testOutput[i]*ScaleValue);
    }


}

 /** \endlink */
